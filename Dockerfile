# Stage 1 (to create a "build" image)
FROM golang:1.10.1 AS builder
RUN go version

WORKDIR /go/src/gitlab.com/sureshkumar.bv/reportGen
COPY . .
#RUN set -x && \
#        go get github.com/golang/dep/cmd/dep && \
#        dep ensure -v
RUN CGO_ENABLED=0 GOOS=linux go build -a -o eventservice

#Stage 2 (Take a scratch image and create docker image)
FROM scratch
WORKDIR /app
COPY --from=builder /go/src/gitlab.com/sureshkumar.bv/reportGen/eventservice /app/
CMD ["./eventservice"]
